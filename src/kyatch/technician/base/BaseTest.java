package kyatch.technician.base;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;

public class BaseTest {
	public static AndroidDriver<WebElement> driver;
//	public static final boolean OPEN_KEYBOARD = false;
//	public static final boolean CLOSE_KEYBOARD = true;
//	protected boolean isKeyboard = CLOSE_KEYBOARD;

	@BeforeTest
	public void setUp() throws MalformedURLException {

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName", "smartron");
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "Android");
		capabilities.setCapability(CapabilityType.VERSION, "6.0");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("unicodeKeyboard", true);
		capabilities.setCapability("resetKeyboard", true);
		capabilities.setCapability("appPackage", "com.kyatch.serviceman");
		capabilities.setCapability("appActivity",
				"com.kyatch.serviceman.app.SplashActivity");

		driver = new AndroidDriver<WebElement>(new URL(
				"http://127.0.0.1:4723/wd/hub"), capabilities);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

	}

}
