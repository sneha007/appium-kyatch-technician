package com.kyatch.Technician.Tests;

import kyatch.technician.base.BaseTest;

import org.testng.annotations.Test;

import com.kyatch.Technician.Pages.AlertMessageOnSplashScreeen;
import com.kyatch.Technician.Pages.LoginPage;
import com.kyatch.Technician.Pages.SignUp;
import com.kyatch.Technician.Pages.UploadFile;
import com.kyatch.Technician.Pages.VerifyOTP;

public class TestAlertMessage extends BaseTest {

	AlertMessageOnSplashScreeen objAlertMessageOnSplashScreeen;
	LoginPage objLoginPage;
	UploadFile objUploadFile;
	SignUp objSignup;
	VerifyOTP objVerifyOTP;

	@Test(testName = "test_Alert_Correct")
	public void testAlertCorrect() throws InterruptedException {

		objAlertMessageOnSplashScreeen = new AlertMessageOnSplashScreeen();
		objAlertMessageOnSplashScreeen.clickOnAlert();
		System.out.println("Allowed Permissions");
		Thread.sleep(2000);

		
//		 objSignup = new SignUp();
//		 objSignup.registrationPage("sathish", "kumar", "9845664466",
//		 "test_16@gmail.com", "123456", "123456");
//		 Thread.sleep(2000);

//		 objVerifyOTP = new VerifyOTP();
//		 objVerifyOTP.verifyOTP();
//		 Thread.sleep(2000);

		objLoginPage = new LoginPage();
		objLoginPage.loginToKyatch("test_16@gmail.com", "123456");
		Thread.sleep(2000);
		System.out.println("successfully login");

//		objVerifyOTP = new VerifyOTP();
//		Thread.sleep(10000);
//		objVerifyOTP.verifyOTP();
//		System.out.println("successfully verified");

		objUploadFile = new UploadFile();
		Thread.sleep(10000);
		objUploadFile.updateProfile("1995-08-13","1318 knolls creek dr", "dalville", "94506");
		System.out.println("Trying to upload file");

	}

}
