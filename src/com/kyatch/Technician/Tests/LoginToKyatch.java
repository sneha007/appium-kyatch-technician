package com.kyatch.Technician.Tests;

import kyatch.technician.base.BaseTest;

import org.testng.annotations.Test;

import com.kyatch.Technician.Pages.LoginPage;

public class LoginToKyatch extends BaseTest {
	
	LoginPage objLoginPage;
	
	@Test(testName = "Login_to_the_App")
	public void loginToApplication() throws InterruptedException{
		objLoginPage = new LoginPage();
		objLoginPage.loginToKyatch("test_01@gmail.com", "123456");
	}

}
