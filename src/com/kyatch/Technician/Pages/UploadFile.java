package com.kyatch.Technician.Pages;

import kyatch.technician.base.BaseTest;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class UploadFile extends BaseTest {
@AndroidFindBy(id = "com.kyatch.serviceman:id/profile_dob_editText")
WebElement dob;

//@AndroidFindBy(id = "com.kyatch.serviceman:id/dialog_bank_ssn")
//WebElement ssn;

@AndroidFindBy(id = "com.kyatch.serviceman:id/dialog_bank_street")
WebElement bank_street;

@AndroidFindBy(id = "com.kyatch.serviceman:id/dialog_bank_location")
WebElement bank_locality;

@AndroidFindBy(id = "com.kyatch.serviceman:id/sp_us_regions")
WebElement bank_region;

@AndroidFindBy(xpath = "//android.widget.TextView[@text='California']")
WebElement region;
	
@AndroidFindBy(id = "com.kyatch.serviceman:id/dialog_bank_postal_code")
WebElement postalcode;
	
@AndroidFindBy(id = "com.kyatch.serviceman:id/upload_id_proof")
WebElement uploadfile;


@AndroidFindBy(id = "android:id/text1")
WebElement techniciantype;

@AndroidFindBy(xpath = "//android.widget.TextView[@text='Student']")
WebElement student_type;

@AndroidFindBy(xpath = "//android.widget.TextView[@text='Select College']")
WebElement select_college;

@AndroidFindBy(xpath = "//android.widget.CheckedTextView[@text='Massachusetts Institute of Technology']")
WebElement college_name;

@AndroidFindBy(id = "com.kyatch.serviceman:id/iv_select_proof_image")
WebElement select_camera_button;

@AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
WebElement toAllow_photos;

@AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
WebElement tocapture_pictures;

@AndroidFindBy(xpath = "//android.widget.ImageView[@bounds='[99,1460][243,1604]']")
WebElement select_gallery;

@AndroidFindBy(id = "com.kyatch.serviceman:id/tv_proof_update")
WebElement continue_button;

@AndroidFindBy(id = "com.kyatch.serviceman:id/tv_profile_update")
WebElement next_button;



public UploadFile() {

	PageFactory.initElements(new AppiumFieldDecorator(driver), this);
}

public void updateProfile(String birthdate,String street,String locality,String pincode) throws InterruptedException{
	uploadfile.click();
	dob.sendKeys(birthdate);
//	Thread.sleep(2000);
//	ssn.sendKeys(security_number);
//	Thread.sleep(2000);
	bank_street.sendKeys(street);
	
	bank_locality.sendKeys(locality);
	bank_region.click();
	region.click();
	postalcode.sendKeys(pincode);		
	next_button.click();
	Thread.sleep(5000);
//	attachidproof.click();
	techniciantype.click();
	System.out.println("selected technician type");
	student_type.click();
	System.out.println("selected technician type as student");
	select_college.click();
	System.out.println("selected college...!!!");
	college_name.click();
	System.out.println("College name is" +college_name);
	select_camera_button.click();
	System.out.println("selected camera button");
	toAllow_photos.click();
	System.out.println("Allowing permissions for photos");
	tocapture_pictures.click();
	select_gallery.click();
	select_gallery.sendKeys("/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20170716-WA0007.jpg");
	System.out.println("Selected Gallery");
	Thread.sleep(10000);
	continue_button.click();	
	      
}

}
