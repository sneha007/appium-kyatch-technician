package com.kyatch.Technician.Pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import kyatch.technician.base.BaseTest;

public class LoginPage extends BaseTest{
	
	@AndroidFindBy(id = "com.kyatch.serviceman:id/login_email_editText")
	WebElement email;

	@AndroidFindBy(id = "com.kyatch.serviceman:id/login_password_editText")
	WebElement password;
	
	@AndroidFindBy(id = "com.kyatch.serviceman:id/login_button")
	WebElement signIn_Button;

	public LoginPage() {

		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		System.out.println("found pagefactory method");
	}
	
	// Get the Email field
	public void emailTextField(String emailId) throws InterruptedException {
		email.sendKeys(emailId);
//		driver.hideKeyboard();
		//driver.pressKeyCode(AndroidKeyCode.BACK);
		Thread.sleep(3000);

	}
	
	//Get the Password field
	public void passwordTextField(String Password) throws InterruptedException{
	    Actions builder = new Actions(driver);
		builder.moveToElement(password);
		password.sendKeys(Password);
//		driver.hideKeyboard();
		Thread.sleep(1000);

	}
	
	//Click on signIn to login the page
	public void clickOnSignIn(){
		signIn_Button.click();
	}
	
	public void loginToKyatch(String emailId,String Password) throws InterruptedException{
		this.emailTextField(emailId);
		this.passwordTextField(Password);	
		this.signIn_Button.click();

		
	}
	
}
