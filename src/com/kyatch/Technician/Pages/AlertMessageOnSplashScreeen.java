package com.kyatch.Technician.Pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import kyatch.technician.base.BaseTest;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class AlertMessageOnSplashScreeen extends BaseTest {

	
	@AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
	WebElement toAllow;

	public AlertMessageOnSplashScreeen() {

		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		System.out.println("found pagefactory method");
	}

	// Click Allow Text
	public void clickOnAlert() {
		try {

			System.out.println("Clicking on Allow option under permissions ");

			toAllow.click();

			System.out.println("Clicked on Allow option under permissions ");

		} catch (Exception e) {
		}
	}
}
