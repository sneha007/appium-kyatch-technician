package com.kyatch.Technician.Pages;

import kyatch.technician.base.BaseTest;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class VerifyOTP extends BaseTest{
	@AndroidFindBy(id = "com.kyatch.serviceman:id/btn_forgot")
	WebElement verify_OTP_button;
	
 public VerifyOTP() {
		// TODO Auto-generated constructor stub
	 PageFactory.initElements(new AppiumFieldDecorator(driver), this);

	}
 
 public void verifyOTP(){
	 
    verify_OTP_button.click();
 }

}
