package com.kyatch.Technician.Pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import kyatch.technician.base.BaseTest;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class SignUp extends BaseTest {

	@AndroidFindBy(id = "com.kyatch.serviceman:id/login_sign_up_textView")
	WebElement signup_button;

	@AndroidFindBy(id = "com.kyatch.serviceman:id/profile_first_name_editText")
	WebElement FN;

	@AndroidFindBy(id = "com.kyatch.serviceman:id/profile_last_name_editText")
	WebElement LN;

	@AndroidFindBy(id = "com.kyatch.serviceman:id/profile_phone_editText")
	WebElement phone_number;

	@AndroidFindBy(id = "com.kyatch.serviceman:id/profile_email_editText")
	WebElement emailid;

	@AndroidFindBy(id = "com.kyatch.serviceman:id/profile_password_editText")
	WebElement password;

	@AndroidFindBy(id = "com.kyatch.serviceman:id/profile_confirm_pwd__editText")
	WebElement confirm_password;

	@AndroidFindBy(id = "com.kyatch.serviceman:id/profile_create_button")
	WebElement create_account_button;
	

	@AndroidFindBy(id = "com.kyatch.serviceman:id/btn_forgot")
	WebElement verify_OTP_button;
	
	public SignUp() {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	public void firstName(String first_name) throws InterruptedException {
		FN.sendKeys(first_name);
//		driver.hideKeyboard();
		Thread.sleep(3000);

	}

	public void lastName(String last_name) throws InterruptedException {
		LN.sendKeys(last_name);
//		driver.hideKeyboard();
		Thread.sleep(3000);

	}

	public void phoneNumber(String phonenumber) throws InterruptedException {
		phone_number.sendKeys(phonenumber);
//		driver.pressKeyCode(AndroidKeyCode.BACK);
		Thread.sleep(3000);

	}

	public void email(String emailId) throws InterruptedException {
		emailid.sendKeys(emailId);
//     	driver.hideKeyboard();
		Thread.sleep(3000);

	}

	public void password(String pass_word) throws InterruptedException {
		password.sendKeys(pass_word);
		driver.hideKeyboard();
		Thread.sleep(3000);

	}

	public void confirmPassword(String confirm_pass_word)
			throws InterruptedException {
		confirm_password.sendKeys(confirm_pass_word);
//		driver.hideKeyboard();
		Thread.sleep(3000);
	}

	public void registrationPage(String first_name, String last_name,
			String phonenumber, String emailId, String pass_word,
			String confirm_pass_word) throws InterruptedException {
		// TODO Auto-generated method stub

		this.signup_button.click();
		this.firstName(first_name);
		this.lastName(last_name);
		this.phoneNumber(phonenumber);
		this.email(emailId);
		this.password(pass_word);
		this.confirmPassword(confirm_pass_word);
		this.create_account_button.click();
		
		

	}

}
